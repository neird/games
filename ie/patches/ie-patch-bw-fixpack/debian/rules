#!/usr/bin/make -f
#-*- makefile -*-
# Made with the aid of dh_make, by Craig Small
# Sample debian/rules that uses debhelper. GNU copyright 1997 by Joey Hess.
# Some lines taken from debmake, by Christoph Lameter.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

PATCH_NAME := bw-fixpack
PATCH_VERSION := $(shell dpkg-parsechangelog | sed -ne 's/^Version: \(\([0-9]\+\):\)\?\(.*\)-.*/\3/p')
PATCH_ARCHIVE := "BiG World Fixpack v"$(PATCH_VERSION).7z
PATCH_URL := "http://downloads.spellholdstudios.net/"$(PATCH_ARCHIVE)
PATCH_TARBALL := ie-patch-$(PATCH_NAME)_$(PATCH_VERSION).orig.tar.gz

build: build-stamp
	test -f bw-fixpack.dep && exit 0 ; \
	bash linux/trim.sh ; \
	bash linux/bwp-convert-patches.sh > bw-fixpack.dep

binary: build

build-stamp:
	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f install-stamp build-stamp
	dh_clean

install: install-stamp
	mkdir -p $$(pwd)/debian/ie-patch-$(PATCH_NAME)/usr/share/ie/patches
	mkdir -p $$(pwd)/debian/ie-patch-$(PATCH_NAME)/usr/share/ie/deps
	mv bw-fixpack.dep $$(pwd)/debian/ie-patch-$(PATCH_NAME)/usr/share/ie/deps
	ls | grep -vE "^debian|linux$$" | tr "\n" "\0" | xargs -n 1 -0 bash -c 'cp -r "$$0" $$(pwd)/debian/ie-patch-$(PATCH_NAME)/usr/share/ie/patches'
	rm -f $$(pwd)/debian/ie-patch-$(PATCH_NAME)/usr/share/ie/patches/*-stamp
	rm -f $$(pwd)/debian/ie-patch-$(PATCH_NAME)/usr/share/ie/patches/*.log

install-stamp:
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs
	dh_install
	touch install-stamp

# Build architecture-independent files here.
binary-indep: build install
	dh_testdir
	dh_testroot
	dh_installdebconf
	dh_installdocs
	dh_installdefoma
	dh_installchangelogs 
	dh_lintian
	dh_link
	dh_strip
	dh_compress
	dh_fixperms
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

# Build architecture-dependent files here.
binary-arch: build install
# We have nothing to do by default.

get-orig-source:
	rm -rf get-orig-source $(PATCH_TARBALL)
	mkdir get-orig-source
	cd get-orig-source ; \
	wget -c $(PATCH_URL) --content-disposition ; \
	mkdir ie-patch-$(PATCH_NAME)_$(PATCH_VERSION).orig ; cd ie-patch-$(PATCH_NAME)_$(PATCH_VERSION).orig ; \
	7z x ../$(PATCH_ARCHIVE) ; mv "BiG World Fixpack"/* . ; rmdir "BiG World Fixpack" ; find | sort -r | rename 's%/([^/]+)$$%/\L$$1%'
	GZIP='--best --no-name' tar czf $(PATCH_TARBALL) -C get-orig-source ie-patch-$(PATCH_NAME)_$(PATCH_VERSION).orig
	echo "  "$(PATCH_TARBALL)" created; move it to the right destination to build the package"
	rm -rf get-orig-source

binary: binary-indep binary-arch

.PHONY: build clean binary-indep binary-arch binary get-orig-source
