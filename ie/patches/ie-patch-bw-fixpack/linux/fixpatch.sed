#restore relative filename in patch header
2s/^[+]{3} .+(patched files|new)\\(.+)/+++ \2/
2s/_depends\\[^\\]+\\//

#lowercase filename in patch header
1,2s/^([-]{3}|[+]{3}) ([^\t]+)\t/\1 \L\2\t/

#replace \ with / in patch header
1,2s/\\/\//g

#change filename in patch header for main tp2 since it is relocated
1,2s/^([-]{3}|[+]{3}) ([^/]+[/])?(setup-)?([^/]+).tp2/\1 setup-\4.tp2/

#drop dos line ending in patch header
/^([-]{3}|[+]{3})/s/\x0D$//