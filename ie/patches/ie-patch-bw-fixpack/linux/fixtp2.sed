# Since we removed all exe files remove tp2 references too
s/COPY .*Setup.*exe.*//gi
s/AT_EXIT .*Setup.*exe.*//gi

# Relocate tp2
s%((REQUIRE_COMPONENT|FORBID_COMPONENT|MOD_IS_INSTALLED) +~ *)([^ ]+[/\])?(setup-)?([^ ]+[.]tp2)%\1setup-\5%gi