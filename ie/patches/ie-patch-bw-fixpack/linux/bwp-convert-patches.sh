FIND="find -regextype posix-extended ! -regex '^\./(debian|\.pc|linux).*'"
PATCH_NAME_PREFIX='bw-fixpack-'

DIRS=`eval $FIND | grep -E '(_copy|_delete|_rename|.*[.]patch)$' | sed -r "s/(.+)\/[^/]+$/\1/" | sed -r "s/.\///" | sort -r | uniq`
for DIR in $DIRS ; do
  MOD=$DIR
  PATCH=${PATCH_NAME_PREFIX}${MOD}

  DDIR=`echo ${DIR} | grep -Eo "^[^/]+"`
  if [ "x$DDIR" == "x$DIR" ] ; then
    echo "patch $PATCH requires and recommended by mod $MOD"
    echo
  fi

  if [ -d ${DIR}/_copy ] ; then
    mkdir -p ${DIR}/copy/${DDIR}
    mv -f ${DIR}/_copy/* ${DIR}/copy/${DDIR}
    rmdir ${DIR}/_copy
  fi

  if [ -f ${DIR}/_rename ] ; then
    dos2unix ${DIR}/_rename
    D=$(echo ${DDIR} | sed -r "s/\&/\\\&/")
    cat ${DIR}/_rename | sed -r 's/\\/\//g' | sed -r "s/^((\S+[/])?[^/[:space:]]+)\s+(.+)/${D}\/\1\t${D}\/\2\3/" | tr "A-Z" "a-z" | grep -vE "[.]exe" > ${DIR}/rename
    rm ${DIR}/_rename
  fi

  if [ -f ${DIR}/_delete ] ; then
    dos2unix ${DIR}/_delete
    cat ${DIR}/_delete | sed -r 's/\\/\//g' | sed -r "s/^/${DDIR}\//" | tr "A-Z" "a-z" | grep -vE "[.]exe"> ${DIR}/delete
    rm ${DIR}/_delete
  fi

  mkdir ${DIR}/patch
  mv -f ${DIR}/*.patch ${DIR}/patch
  rmdir --ignore-fail-on-non-empty ${DIR}/patch
done

DEPS=$(eval $FIND -name _depends | sort -r)
for DEP in $DEPS ; do
  #FIXME should be MOD_DIR
  MOD1=`echo $DEP | sed -r 's%.+/([^/]+)/_depends%\1%'`
  MOD2=`ls $DEP`

  PATCH="${PATCH_NAME_PREFIX}${MOD1}_with_${MOD2}"
  PATCH1="${PATCH_NAME_PREFIX}${MOD1}"

  echo "patch $PATCH requires and recommended by mod $MOD1 and mod $MOD2"
  echo "patch $PATCH requires patch $PATCH1"
  echo

  mkdir -p ${MOD1}_with_${MOD2}
  mv ${DEP}/${MOD2}/* ${MOD1}_with_${MOD2}
  rmdir ${DEP}/${MOD2} ${DEP}
done

DEPS=$(eval $FIND -name _depends_not | sort -r)
for DEP in $DEPS ; do
  MOD1=`echo $DEP | sed -r 's%.+/([^/]+)/_depends_not%\1%'`
  MOD2=`ls $DEP`

  PATCH="${PATCH_NAME_PREFIX}${MOD1}_without_${MOD2}"
  PATCH1="${PATCH_NAME_PREFIX}${MOD1}"

  echo "patch $PATCH requires and recommended by mod $MOD1"
  echo "patch $PATCH forbids mod $MOD2"
  echo "patch $PATCH requires patch $PATCH1"
  echo

  mkdir -p ${MOD1}_without_${MOD2}
  mv ${DEP}/${MOD2}/* ${MOD1}_without_${MOD2}
  rmdir ${DEP}/${MOD2} ${DEP}
done

DIRS=$(eval $FIND -regex '^\./[^/]+' -type d | sed -r "s/^\.\///")
for DIR in $DIRS ; do
  mv $DIR $PATCH_NAME_PREFIX$DIR
done

eval $FIND -name '*.exe' -print0 | xargs -0 -n 1 rm -f
eval $FIND -name '*.patch' -print0 | xargs -0 -n 1 sed -i -r -f linux/fixpatch.sed
eval $FIND -name '*.tp?.patch' -print0 | xargs -0 -n 1 sed -i -r -f linux/fixtp2.sed
eval $FIND ! -regex '.*/patch/setup-.*' -name '*.tp2.patch' -print0 | xargs -n 1 -0 bash -c 'mv "$0" $(echo "$0" | sed -r "s%/patch/([^/]+[.]tp2)%/patch/setup-\1%")'
