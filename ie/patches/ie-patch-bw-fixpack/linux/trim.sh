# unneeded special case
mv "_ad&l" "ad&l"

#remove ?
rm -rf _*

#remove description and installer script
rm -f "big world fixpack.bat" "big world fixpack read me.txt" ; \

#remove files which are not used
rm -rf "bpv180/use this one only if bp ai gets installed, but without the spellhold-component!"
rm -rf "dsoa/sar0700.baf.sqx"
rm -rf "mod backups"

#remove ?
rm -rf "ts/_depends/kachiko bonus portrait" && rmdir --ignore-fail-on-non-empty ts/_depends
rm -rf "ts/_depends_not/kachiko bonus portrait" && rmdir --ignore-fail-on-non-empty ts/_depends_not

#move misplaced ?
mv -f "sos/sovereign" sovereign

#move misplaced ?
mv "1sylm/_depends/big world installpack"/* 1sylm
rm -rf "1sylm/_depends/big world installpack" && rmdir --ignore-fail-on-non-empty 1sylm/_depends
rm -rf "1sylm/_depends_not/big world installpack" && rmdir --ignore-fail-on-non-empty 1sylm/_depends_not

#move optional
mv "bw_herbs_bg2/_optional/improved animations/"* bw_herbs_bg2
rm -rf bw_herbs_bg2/_optional bw_herbs_bg2/_description
mv "bw_herbs/_optional/improved animations/"* bw_herbs
rm -rf bw_herbs/_optional bw_herbs/_description
