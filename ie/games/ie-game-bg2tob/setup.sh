#!/bin/bash

OPTION=$1
GAMEDIR=$2

if [ "x$DATADIR" == "x" ] ; then
  DATADIR=/usr/share/ie
fi

case "$OPTION" in
  install)
    LANGUAGE=$3
    if [ "x$LANGUAGE" == "x" ] ; then
      LANGUAGE="english"
    fi

    for LNG in common $LANGUAGE ; do
      for FILE in keymap.ini language.txt ; do
        if [ -f $DATADIR/games/bg2tob/data/$LNG/$FILE ] ; then
          cp -f $DATADIR/games/bg2tob/data/$LNG/$FILE $GAMEDIR
        fi
      done
    done

    #FIXME
    #rm override/baldur.bcs
  ;;

  setup-wine)
    SIZE_X=$3
    SIZE_Y=$4

    LANGUAGE=$5
    if [ "x$LANGUAGE" == "x" ] ; then
      LANGUAGE="english"
    fi

    if [ "x$WINEPREFIX" == "x" ] ; then
      echo "Define WINEPREFIX" >&2
      exit 1
    else
      if [ ! -x "$WINEPREFIX/drive_c" ] ; then
        echo "Broken wine installation\n" >&2
        exit 1
      fi

      cd $GAMEDIR

      #prepare config
      cat $DATADIR/games/bg2tob/baldur.ini | \
      sed -r "s/%HD0%/C:\\\\GAME\\\/" | \
      sed -r "s/%CD[0-9]+%/C:\\\\GAME\\\\/" > \
      baldur.ini

      #copy all other files
      for LNG in common $LANGUAGE ; do
        for FILE in bgmain.exe bgconfig.exe charview.exe ; do
          if [ -f $DATADIR/games/bg2tob/data/$LNG/$FILE ] ; then
            cp -f $DATADIR/games/bg2tob/data/$LNG/$FILE .
          fi
        done
      done

      #add required CD data
      mkdir -p $WINEPREFIX/drive_d/CD5
      ln -sf $DATADIR/games/bg2tob/data/common/{data,movies} $WINEPREFIX/drive_d/CD5
    fi
  ;;

  setup-gemrb)
    SIZE_X=$3
    SIZE_Y=$4

    LANGUAGE=$5
    if [ "x$LANGUAGE" == "x" ] ; then
      LANGUAGE="english"
    fi

    cd $GAMEDIR

    #prepare config
    cat $DATADIR/linux-tools/gemrb.cfg | \
    sed -r "s/%HD0%/./" | \
    sed -r "s/%CD[0-5]+%/./" | \
    sed -r "s/%SIZE_X%/$SIZE_X/" | \
    sed -r "s/%SIZE_Y%/$SIZE_Y/" | \
    sed -r "s/%GAME_TYPE%/bg2/" | \
    sed -r "s/%GAME_NAME%/Baldur's Gate II: Throne of Bhaal/" > \
    .gemrb/gemrb.cfg
  ;;

  *)
    echo "Usage: $0 {install|setup-wine|setup-gemrb}" >&2
    exit 1
  ;;
esac
