#!/bin/bash

OPTION=$1
GAMEDIR=$2

if [ "x$DATADIR" == "x" ] ; then
  DATADIR=/usr/share/ie
fi

case "$OPTION" in
  install)
    LANGUAGE=$3
    if [ "x$LANGUAGE" == "x" ] ; then
      LANGUAGE="english"
    fi

    for LNG in common $LANGUAGE ; do
      for FILE in autonote.ini beast.ini keymap.ini layout.ini quests.ini var.var ; do
        if [ -f $DATADIR/games/pst/data/$LNG/$FILE ] ; then
          cp -f $DATADIR/games/pst/data/$LNG/$FILE $GAMEDIR
        fi
      done
    done
  ;;

  setup-wine)
    SIZE_X=$3
    SIZE_Y=$4

    LANGUAGE=$5
    if [ "x$LANGUAGE" == "x" ] ; then
      LANGUAGE="english"
    fi

    if [ "x$WINEPREFIX" == "x" ] ; then
      echo "Define WINEPREFIX" >&2
      exit 1
    else
      if [ ! -x "$WINEPREFIX/drive_c" ] ; then
        echo "Broken wine installation\n" >&2
        exit 1
      fi

      cd $GAMEDIR

      #prepare config
      cat $DATADIR/games/pst/torment.ini | \
      sed -r "s/%HD0%/C:\\\\GAME\\\\/" | \
      sed -r "s/%CD[1-3]+%/C:\\\\GAME\\\\DATA\\\\/" | \
      sed -r "s/%CD4+%/DATA/" > \
      torment.ini

      #copy all other files
      for LNG in common $LANGUAGE ; do
        for FILE in torment.exe ; do
          if [ -f $DATADIR/games/pst/data/$LNG/$FILE ] ; then
            cp -f $DATADIR/games/pst/data/$LNG/$FILE .
          fi
        done
      done

      #add required CD data
      mkdir -p $WINEPREFIX/drive_d/cd2
      ln -s $DATADIR/games/pst/data/common/data/genmova.bif $WINEPREFIX/drive_d/cd2
    fi
  ;;

  setup-gemrb)
    SIZE_X=$3
    SIZE_Y=$4

    LANGUAGE=$5
    if [ "x$LANGUAGE" == "x" ] ; then
      LANGUAGE="english"
    fi

    cd $GAMEDIR

    #prepare config
    cat $DATADIR/linux-tools/gemrb.cfg | \
    sed -r "s/%HD0%/./" | \
    sed -r "s/%CD[0-3]+%/./" | \
    sed -r "s/%CD4+%/data/" | \
    sed -r "s/%SIZE_X%/$SIZE_X/" | \
    sed -r "s/%SIZE_Y%/$SIZE_Y/" | \
    sed -r "s/%GAME_TYPE%/pst/" | \
    sed -r "s/%GAME_NAME%/Planescape: Torment/" > \
    .gemrb/gemrb.cfg
  ;;

  *)
    echo "Usage: $0 {install|setup-wine|setup-gemrb}" >&2
    exit 1
  ;;
esac
