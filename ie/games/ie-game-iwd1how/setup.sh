#!/bin/bash

OPTION=$1
GAMEDIR=$2

if [ "x$DATADIR" == "x" ] ; then
  DATADIR=/usr/share/ie
fi

case "$OPTION" in
  install)
    #remove some override files like in CD install
    OLD_OVERRIDES="
    ar6009.are ar7002.are
    d4yuanp.bcs d4yuninf.bcs eepoqcng.bcs eheverar.bcs lddgnom2.bcs ldfleez1.bcs ldirngol.bcs udnorlin.bcs wtdoor2.bcs wtgiant1.bcs wtslvrun.bcs
    ar1006sr.bmp
    ddavin.dlg dfengla.dlg dkayless.dlg dmytos.dlg dnorlino.dlg dogre.dlg
    boot01.itm potn41.itm
    spwi104.spl
    "

    for OVERRIDE in $OLD_OVERRIDES ; do
      if [ -e "$GAMEDIR/override/$OVERRIDE" ] ; then
        rm "$GAMEDIR/override/$OVERRIDE"
      fi
    done

    for LNG in common $LANGUAGE ; do
      for FILE in language.ini ; do
        if [ -f $DATADIR/games/iwd1how/data/$LNG/$FILE ] ; then
          cp -f $DATADIR/games/iwd1how/data/$LNG/$FILE $GAMEDIR
        fi
      done
    done
  ;;

  setup-wine)
    SIZE_X=$3
    SIZE_Y=$4

    LANGUAGE=$5
    if [ "x$LANGUAGE" == "x" ] ; then
      LANGUAGE="english"
    fi

    if [ "x$WINEPREFIX" == "x" ] ; then
      echo "Define WINEPREFIX" >&2
      exit 1
    else
      if [ ! -x "$WINEPREFIX/drive_c" ] ; then
        echo "Broken wine installation\n" >&2
        exit 1
      fi

      cd $GAMEDIR

      #prepare config
      cat $DATADIR/games/iwd1how/icewind.ini | \
      sed -r "s/%HD0%/C:\\\\GAME\\\/" | \
      sed -r "s/%CD[1-3]+%/C:\\\\GAME\\\\/" > \
      icewind.ini

      #copy all other files
      for LNG in common $LANGUAGE ; do
        for FILE in idmain.exe config.exe ; do
          if [ -f $DATADIR/games/iwd1how/data/$LNG/$FILE ] ; then
            cp -f $DATADIR/games/iwd1how/data/$LNG/$FILE .
          fi
        done
      done

      #add required CD data
      mkdir -p $WINEPREFIX/drive_d/CD3/data
      touch $WINEPREFIX/drive_d/CD3/data/IWDCD.3
    fi
  ;;

  setup-gemrb)
    SIZE_X=$3
    SIZE_Y=$4

    LANGUAGE=$5
    if [ "x$LANGUAGE" == "x" ] ; then
      LANGUAGE="english"
    fi

    cd $GAMEDIR

    #prepare config
    cat $DATADIR/linux-tools/gemrb.cfg | \
    sed -r "s/%HD0%/./" | \
    sed -r "s/%CD[0-5]+%/./" | \
    sed -r "s/%SIZE_X%/$SIZE_X/" | \
    sed -r "s/%SIZE_Y%/$SIZE_Y/" | \
    sed -r "s/%GAME_TYPE%/how/" | \
    sed -r "s/%GAME_NAME%/Icewind Dale: Heart of Winter/" > \
    .gemrb/gemrb.cfg
  ;;

  *)
    echo "Usage: $0 {install|setup-wine|setup-gemrb}" >&2
    exit 1
  ;;
esac
