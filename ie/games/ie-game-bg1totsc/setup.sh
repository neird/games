#!/bin/bash

OPTION=$1
GAMEDIR=$2

if [ "x$DATADIR" == "x" ] ; then
  DATADIR=/usr/share/ie
fi

case "$OPTION" in
  install)
    LANGUAGE=$3
    if [ "x$LANGUAGE" == "x" ] ; then
      LANGUAGE="english"
    fi

    #remove some override files like in CD install
    OLD_OVERRIDES="
    ar2300.are
    actflam.bcs ar1900.bcs ar2100.bcs ar3300.bcs ar3352.bcs drizzt.bcs gnoll5.bcs gnolldr.bcs sarevok.bcs yeslick.bcs
    albert.cre belt.cre benjy.cre bjorni.cre brage2.cre branwe.cre davaeo.cre drizzt.cre firebe.cre flam13.cre jaheir6.cre
    liia.cre mulahe.cre nalin.cre nikola.cre ragefa.cre ramazi.cre schlum.cre skelwa.cre skelwa02.cre skelwa03.cre voltin.cre
    albert.dlg amnise.dlg bendal.dlg cadder.dlg caedmo.dlg calaha.dlg childulg.dlg dink.dlg divine.dlg dradee.dlg evalt.dlg farthi.dlg
    fenrus.dlg fenten.dlg flam2.dlg ftowbez.dlg ftown2.dlg gandol.dlg gatewa2.dlg gatewere.dlg gnarl.dlg gnoll5.dlg husam.dlg islsir.dlg
    jorin.dlg kryla.dlg lahl.dlg lothan.dlg maltz.dlg marale.dlg memnis.dlg mtowbez.dlg nadin.dlg narlen.dlg ogrelead.dlg oublek.dlg
    palin.dlg ragefa.dlg ramazi.dlg sarevo.dlg shoal.dlg silenc.dlg taloun.dlg tethto2.dlg thalan.dlg therel.dlg tick.dlg tremai.dlg
    ulcast.dlg ulraun.dlg unshey.dlg viconi.dlg voleta.dlg
    bolt02.itm bolt03.itm bolt04.itm bolt05.itm bolt06.itm boot02.itm bow01.itm bow02.itm chan06.itm flam01.itm leat08.itm misc72.itm
    misc89.itm potn40.itm ring09.itm schlum1.itm sw2h06.itm
    sppr403.spl spwi107.spl spwi116.spl spwi313.spl
    tem2304.sto tem4802.sto
    "

    for OVERRIDE in $OLD_OVERRIDES ; do
      if [ -e "$GAMEDIR/override/$OVERRIDE" ] ; then
        rm "$GAMEDIR/override/$OVERRIDE"
      fi
    done
  ;;

  setup-wine)
    SIZE_X=$3
    SIZE_Y=$4

    LANGUAGE=$5
    if [ "x$LANGUAGE" == "x" ] ; then
      LANGUAGE="english"
    fi

    if [ "x$WINEPREFIX" == "x" ] ; then
      echo "Define WINEPREFIX" >&2
      exit 1
    else
      if [ ! -x "$WINEPREFIX/drive_c" ] ; then
        echo "Broken wine installation\n" >&2
        exit 1
      fi

      cd $GAMEDIR

      #prepare config
      cat $DATADIR/games/bg1totsc/baldur.ini | \
      sed -r "s/%HD0%/C:\\\\GAME\\\/" | \
      sed -r "s/%CD[0-5]+%/D:\\\\CD1\\\\/" | \
      sed -r "s/%CD6+%/D:\\\\CD6\\\\/" > \
      baldur.ini

      #copy all other files
      for LNG in common $LANGUAGE ; do
        for FILE in bgmain.exe bgmain2.exe config.exe ; do
          if [ -f $DATADIR/games/bg1totsc/data/$LNG/$FILE ] ; then
            cp -f $DATADIR/games/bg1totsc/data/$LNG/$FILE .
          fi
        done
      done

      #add required CD data
      mkdir -p $WINEPREFIX/drive_d/CD6
      ln -sf $DATADIR/games/bg1totsc/data/common/movies $WINEPREFIX/drive_d/CD6
    fi
  ;;

  setup-gemrb)
    SIZE_X=$3
    SIZE_Y=$4

    LANGUAGE=$5
    if [ "x$LANGUAGE" == "x" ] ; then
      LANGUAGE="english"
    fi

    cd $GAMEDIR

    #prepare config
    cat $DATADIR/linux-tools/gemrb.cfg | \
    sed -r "s/%HD0%/./" | \
    sed -r "s/%CD[0-5]+%/./" | \
    sed -r "s/%SIZE_X%/$SIZE_X/" | \
    sed -r "s/%SIZE_Y%/$SIZE_Y/" | \
    sed -r "s/%GAME_TYPE%/bg1/" | \
    sed -r "s/%GAME_NAME%/Baldur's Gate: Tales of the Sword Coast/" > \
    .gemrb/gemrb.cfg
  ;;

  *)
    echo "Usage: $0 {install|setup-wine|setup-gemrb}" >&2
    exit 1
  ;;
esac
