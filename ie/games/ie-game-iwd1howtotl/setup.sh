#!/bin/bash

OPTION=$1
GAMEDIR=$2

if [ "x$DATADIR" == "x" ] ; then
  DATADIR=/usr/share/ie
fi

case "$OPTION" in
  install)
    LANGUAGE=$3
    if [ "x$LANGUAGE" == "x" ] ; then
      LANGUAGE="english"
    fi
  ;;

  setup-wine)
    SIZE_X=$3
    SIZE_Y=$4

    LANGUAGE=$5
    if [ "x$LANGUAGE" == "x" ] ; then
      LANGUAGE="english"
    fi

    if [ "x$WINEPREFIX" == "x" ] ; then
      echo "Define WINEPREFIX" >&2
      exit 1
    else
      if [ ! -x "$WINEPREFIX/drive_c" ] ; then
        echo "Broken wine installation\n" >&2
        exit 1
      fi

      cd $GAMEDIR

      #copy all other files
      for LNG in common $LANGUAGE ; do
        for FILE in idmain.exe ; do
          if [ -f $DATADIR/games/iwd1howtotl/data/$LNG/$FILE ] ; then
            cp -f $DATADIR/games/iwd1howtotl/data/$LNG/$FILE .
          fi
        done
      done
    fi
  ;;

  setup-gemrb)
    SIZE_X=$3
    SIZE_Y=$4

    LANGUAGE=$5
    if [ "x$LANGUAGE" == "x" ] ; then
      LANGUAGE="english"
    fi

    cd $GAMEDIR

    #prepare config
    cat $DATADIR/linux-tools/gemrb.cfg | \
    sed -r "s/%HD0%/./" | \
    sed -r "s/%CD[0-5]+%/./" | \
    sed -r "s/%SIZE_X%/$SIZE_X/" | \
    sed -r "s/%SIZE_Y%/$SIZE_Y/" | \
    sed -r "s/%GAME_TYPE%/how/" | \
    sed -r "s/%GAME_NAME%/Icewind Dale: Heart of Winter - Trials of the Luremaster/" > \
    .gemrb/gemrb.cfg
  ;;

  *)
    echo "Usage: $0 {install|setup-wine|setup-gemrb}" >&2
    exit 1
  ;;
esac
