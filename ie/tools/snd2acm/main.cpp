#include "acmsound.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>

void usage(){
  fprintf(stderr,"Sound to ACM converter Linux port, neird.neird@gmail.com\n");
  fprintf(stderr,"Usage: snd2acm [-snd_type] snd_file_name acm_file_name\n");
  fprintf(stderr,"  snd_type can be used to specify the sound file format:\n");
  fprintf(stderr,"    -acm  ACM file\n");
  fprintf(stderr,"    -wav  WAV file\n");
  return;
}

int main(int argc, char *argv[]){
  bool wavc_or_acm=true;
  int fhandle;
  FILE *foutp;
  int res;
  char *filein;
  char *fileout;

  if (argc<3){
    usage();
    exit(EXIT_FAILURE);
  }
  if ((strcmp(argv[1],"-wav") == 0) || (strcmp(argv[1],"-acm") == 0)){
    if (argc!=4){
      usage();
      exit(EXIT_FAILURE);
    }
    if (strcmp(argv[1],"-wav") == 0){
      wavc_or_acm=true;
    } else {
      wavc_or_acm=false;
    }
    filein=argv[2];
    fileout=argv[3];
  } else {
    filein=argv[1];
    fileout=argv[2];
  }

  fhandle=open(filein,O_RDONLY);
  if(fhandle<1){
    fprintf(stderr,"Error opening input file %s\n",filein);
    exit(EXIT_FAILURE);
  }

  foutp=fopen(fileout,"wb");
  if(!foutp){
    fprintf(stderr,"Error opening output file %s\n",fileout);
    exit(EXIT_FAILURE);
  }

  res=ConvertWavAcm(fhandle,-1,foutp, wavc_or_acm);

  fclose(foutp);
  close(fhandle);
}

