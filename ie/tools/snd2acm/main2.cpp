#include "acmsound.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>

void usage(){
  fprintf(stderr,"ACM to WAV converter Linux port, neird.neird@gmail.com\n");
  fprintf(stderr,"Usage: acm2snd [ -forcestereo ] acm_file_name snd_file_name\n");
  return;
}

int main(int argc, char *argv[]){
  bool forcestereo=0;
  int fhandle;
  int fhandleout;
  int res;
  char *filein;
  char *fileout;
  unsigned char *memory;
  long samples_written;

  if (argc<3){
    usage();
    exit(EXIT_FAILURE);
  }
  if (strcmp(argv[1],"-forcestereo") == 0){
    forcestereo=1;
    filein=argv[2];
    fileout=argv[3];
  } else {
    filein=argv[1];
    fileout=argv[2];
  }

  fhandle=open(filein,O_RDONLY);
  if(fhandle<1){
    fprintf(stderr,"Error opening input file %s\n",filein);
    exit(EXIT_FAILURE);
  }

  fhandleout=open(fileout, O_RDWR|O_CREAT|O_TRUNC,S_IREAD|S_IWRITE);
  if(fhandleout<1)
  {
    fprintf(stderr,"Error opening output file %s\n",fileout);
    exit(EXIT_FAILURE);
  }

  ConvertAcmWav(fhandle,-1,memory,samples_written, forcestereo);
  res=(write(fhandleout,memory,samples_written)!=samples_written);
  if (res){
    fprintf(stderr,"Error writing output file %s\n",fileout);
    exit(EXIT_FAILURE);
  }
  close(fhandleout);
  close(fhandle);
}
