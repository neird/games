#if !defined(AFX_ACMSOUND_H)
#define AFX_ACMSOUND_H

#include <stdio.h>

int ConvertAcmWav(int fhandle, long maxlen, unsigned char *&memory, long &samples_written, int forcestereo);
int ConvertWavAcm(int fh, long maxlen, FILE *foutp, bool wavc_or_acm);

#endif // !defined(AFX_ACMSOUND_H)