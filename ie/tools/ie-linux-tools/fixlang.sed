# Map german
s/^([0-9]+:)(|.* |.*[()])(german|deutsch|deutsche)($| .*|[()].*)/\1german/

# Map french
s/^([0-9]+:)(|.* |.*[()])(french|francais|fran(\x87|\xe7)ais)($| .*|[()].*)/\1french/

# Map spanish
s/^([0-9]+:)(|.* |.*[()])(spanish|espanol|espa(\xa4|\xf1)ol)($| .*|[()].*)/\1spanish/

# Map italian
s/^([0-9]+:)(|.* |.*[()])(italian|italiano)($| .*|[()].*)/\1italian/

# Map castilian
s/^([0-9]+:)(|.* |.*[()])(castellano|castilian)($| .*|[()].*)/\1castilian/

# Map portiguese
s/^([0-9]+:)(|.* |.*[()])(portuguese)($| .*|[()].*)/\1portuguese/

# Map polish
s/^([0-9]+:)(|.* |.*[()])(polish|polski)($| .*|[()].*)/\1polish/

# Map czech
s/^([0-9]+:)(|.* |.*[()])(czech|cesky)($| .*|[()].*)/\1czech/

# Map russian
s/^([0-9]+:)(|.* |.*[()])(russian)($| .*|[()].*)/\1russian/

# Map japanese
s/^([0-9]+:)(|.* |.*[()])(japanese)($| .*|[()].*)/\1japanese/

# Map korean
s/^([0-9]+:)(|.* |.*[()])(korean)($| .*|[()].*)/\1korean/

# Map chinese
s/^([0-9]+:)(|.* |.*[()])(chinese( |-|\(|\))+traditional|traditional( |-|\(|\))+chinese)($| .*|[()].*)/\1chinese traditional/
s/^([0-9]+:)(|.* |.*[()])(chinese( |-|\(|\))+simplified|simplified( |-|\(|\))+chinese)($| .*|[()].*)/\1chinese simplified/
/^([0-9]+:)chinese (traditional|simplified)$/!s/^([0-9]+:)(|.* |.*[()])(chinese)($| .*|[()].*)/\1chinese/

# Map english
s/^([0-9]+:)(|.* |.*[()])(english( |-|\(|\))+american|american( |-|\(|\))+english)($| .*|[()].*)/\1english american/
s/^([0-9]+:)(|.* |.*[()])(english( |-|\(|\))+international|international( |-|\(|\))+english)($| .*|[()].*)/\1english international/
/^([0-9]+:)english (american|international)$/!s/^([0-9]+:)(|.* |.*[()])(english)($| .*|[()].*)/\1english/
