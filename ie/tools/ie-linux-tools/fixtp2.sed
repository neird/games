# Remove ACTION_IF condition based on ~%WEIDU_OS%
:more5
/ACTION_IF *\(?[~"]%WEIDU_OS%[~"] *(STRING_EQUAL_CASE *[~"][Ww][Ii][Nn]32[~"]( *= *1)?|STRING_COMPARE_CASE *[~"][Ww][Ii][Nn]32[~"] *= *0)\)? (THEN )?BEGIN/! b end5
N
/ACTION_IF *\(?[~"]%WEIDU_OS%[~"] *(STRING_EQUAL_CASE *[~"][Ww][Ii][Nn]32[~"]( *= *1)?|STRING_COMPARE_CASE *[~"][Ww][Ii][Nn]32[~"] *= *0)\)? (THEN )?BEGIN(.|\n)*END +ELSE +(BEGIN|ACTION_IF)/ ! b more5
s/ *ACTION_IF *\(?[~"]%WEIDU_OS%[~"] *(STRING_EQUAL_CASE *[~"][Ww][Ii][Nn]32[~"]( *= *1)?|STRING_COMPARE_CASE *[~"][Ww][Ii][Nn]32[~"] *= *0)\)? (THEN )?BEGIN[^\n]*\n((.*|\n)+)\n(\s*END +ELSE +(BEGIN|ACTION_IF)([^\n]*))/\4\n\6/
T more5

:more6
/\n(( *)END +ELSE +(BEGIN|ACTION_IF)[^\n]*)/! b end5
N
s/\n(( *)END +ELSE +BEGIN[^\n]*)\n.*\n(\2)END//
s/\n((( *)END +ELSE +ACTION_IF)[^\n]*)\n.*\n(\3)END +ELSE +ACTION_IF/\n\2/
s/\n((( *)END +ELSE +ACTION_IF)[^\n]*)\n.*\n(\3)END//
b more6
:end5

# Remove ACTION_IF condition based on ~%WEIDU_ARCH%
:more7
/ACTION_IF *\([~"]%WEIDU_ARCH%[~"] *(STRING_COMPARE_CASE *[~"][Xx]86[~"] *= *0)\) (THEN )?BEGIN/! b end7
N
/ACTION_IF *\([~"]%WEIDU_ARCH%[~"] *(STRING_COMPARE_CASE *[~"][Xx]86[~"] *= *0)\) (THEN )?BEGIN(.|\n)*END +ELSE +BEGIN/ ! b more7
s/ *ACTION_IF *\([~"]%WEIDU_ARCH%[~"] *(STRING_COMPARE_CASE *[~"][Xx]86[~"] *= *0)\) (THEN )?BEGIN[^\n]*\n((.*|\n)+)\n(\s*END +ELSE +BEGIN([^\n]*))/\3\n\5/
T more7

:more8
/\n(( *)END +ELSE +BEGIN[^\n]*)/! b end7
N
s/\n(( *)END +ELSE +BEGIN[^\n]*)\n.*\n(\2)END//
b more8
:end7

# Remove bat relocation commands and fix AT_ calls
:more1
/COPY +([+] +)?~([^~]+[.]bat)~ +~([^~]+[.]bat)~ *(\n|$)/! b end1
$ b end1
s/[/]{2}[^\n$]*COPY +([+] +)?~([^~]+[.]bat)~ +~([^~]+[.]bat)~ *(\n|$)//
N
s/COPY +([+] +)?~([^~]+[.]bat)~ +~([^~]+[.]bat)~ *\n(.*)(AT_(INTERACTIVE_)?(NOW|EXIT|UNINSTALL) +)~\3~/\4\5~\2~/
s/COPY +([+] +)?~([^~]+[.]bat)~ +~([^~]+[.]bat)~ *\n(.*)(AT_(INTERACTIVE_)?(NOW|EXIT|UNINSTALL) +)~\2~/\4\5~\2~/
b more1
:end1
s/COPY +([+] +)?~([^~]+[.]bat)~ +~([^~]+[.]bat)~ *(\n|$)//g

# Convert the .tp2 file to a version that will work using sh instead of bat files
:more4
s/([_-][Ww][Ii][Nn](32)?)?[.][Bb][Aa][Tt](\s|~)/.sh\3/
#s/([_-][Oo][Ss][Xx])[.][Ss][Hh](\s|~)/.sh\2/
#s/([_-][Ll][Ii][Nn]([Uu][Xx])?)[.][Ss][Hh](\s|~)/.sh\2/
t more4

# Remove COPY commands for tisunpack.exe, oggdec.exe, setup-*.exe, respect multiline COPY command
:more2
/\s*COPY\s+.*(tisunpack[.]exe|oggdec[.]exe|[Ss][Ee][Tt][Uu][Pp].*[.]exe)~.*/! b end2
N
s/(\s*COPY(\s|[-]|[+])+)~[^~]+~\s+~[^~]+~\s*\n\s*~/\1~/
t more2
s/(\s*COPY(\s|[-]|[+])+)~[^~]+~\s+~[^~]+~\s*\n((\s|\t)*([^ \t~]|$|\n))/\3/
t more2
:end2

# Replace \ to / in AT_ commands
:more3
s/(AT_(INTERACTIVE_)?(NOW|EXIT|UNINSTALL)\s+~)([^~\\]+)\\([^\\~]+)/\1\4\/\5/
t more3

# Since we removed all exe files remove tp2 references too
s/AT_(NOW|EXIT|UNINSTALL)\s+~.*Setup.*exe.*($|\n)//gi

# Replace local tisunpack, oggdec, sox calls for Linux and OSX
s%(AT_(NOW|EXIT|UNINSTALL)\s+~ *)(sh +)?[^ ]+[/\](tisunpack|oggdec|sox)([.]exe)? %\1\4 %g

# Replace Windows copy command
s%(AT_(NOW|EXIT|UNINSTALL)\s+~ *)copy %\1cp %g

# Add VIEW for html and txt files
s/(AT_(INTERACTIVE_)?(NOW|EXIT|UNINSTALL)\s+~)([Vv][Ii][Ee][Ww] )?(.*)([.](html|htm|txt)~)/\1VIEW \5\6/g

# Relocate tp2
s%((REQUIRE_COMPONENT|FORBID_COMPONENT|MOD_IS_INSTALLED|REQUIRE_FILE) +~ *)([^ ]+[/\])?(setup-)?([^ ]+[.]tp2)%\1setup-\5%gi
