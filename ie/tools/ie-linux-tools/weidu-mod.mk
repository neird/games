#!/usr/bin/make -f
#-*- makefile -*-

MOD_NAME := $(shell dpkg-parsechangelog | sed -ne 's/^Source: ie-mod-\(.\+\)/\1/p')
MOD_VERSION := $(shell dpkg-parsechangelog | sed -ne 's/^Version: \(\([0-9]\+\):\)\?\(.*\)-.*/\3/p')
MOD_TARBALL := ie-mod-$(MOD_NAME)_$(MOD_VERSION).orig.tar.gz
MOD_TP2NAME := $(MOD_NAME)
MOD_DIRNAME := $(MOD_NAME)
MOD_ARCHIVE_ROOTDIR :=

build: build-stamp
	ie-mod-dev patch "$(MOD_TP2NAME)" "$(MOD_DIRNAME)"

build-stamp:
	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f install-stamp build-stamp
	dh_clean

install: install-stamp
	ie-mod-dev install "$(MOD_TP2NAME)" "$(MOD_DIRNAME)" $$(pwd)/debian/ie-mod-$(MOD_NAME)/usr/share/ie/mods/$(MOD_NAME)

install-stamp:
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs
	dh_install
	touch install-stamp

# Build architecture-independent files here.
binary-indep: build install
	dh_testdir
	dh_testroot
	dh_installdebconf
	dh_installdocs
	dh_installchangelogs
	dh_lintian
	dh_link
	dh_strip
	dh_compress
	dh_fixperms
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

# Build architecture-dependent files here.
binary-arch: build install
# We have nothing to do by default.

get-source:
	rm -rf get-orig-source
	mkdir -p get-orig-source
	cd get-orig-source ; \
	wget -c $(MOD_URL) --content-disposition ; \
	touch ../get-source

get-extracted-source: get-source
	cd get-orig-source ; \
	mkdir ie-mod-$(MOD_NAME)_$(MOD_VERSION).orig ; cd ie-mod-$(MOD_NAME)_$(MOD_VERSION).orig ; \
	ie-mod-dev tear ../$(MOD_ARCHIVE) "$(MOD_TP2NAME)" "$(MOD_DIRNAME)" $(MOD_ARCHIVE_ROOTDIR)

post-get-extracted-source:

pre-get-extracted-source:

get-orig-source: pre-get-extracted-source get-extracted-source post-get-extracted-source
	rm -rf $(MOD_TARBALL)
	GZIP='--best --no-name' tar czf $(MOD_TARBALL) -C get-orig-source ie-mod-$(MOD_NAME)_$(MOD_VERSION).orig
	echo "  "$(MOD_TARBALL)" was created, move it to the proper place to build the package"
	rm -rf get-orig-source get-source

binary: binary-indep binary-arch

.PHONY: build clean binary-indep binary-arch binary get-orig-source
