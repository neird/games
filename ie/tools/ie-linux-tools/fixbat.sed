# This command file is to change the dos-style
# commands used by weidu mods into valid linux commands.

# This will not deal with all of the commands, 
# the commands at the top will still need to
# be processed separately.

# dos2unix should be used on the .bat files
# first, because if it isn't, then this file
# will add ^M to the end of each line

# change everything to lower case
y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/

# Change the dos-style %VARNAME% to the unix-style ${VARNAME}
s/%(.*)%/${\1}/g

# Drop set command
s/set ([a-z0-9]+)=(.+)/\1=\2/

# %CD% var in bat has special meaning
s/\$(\{cd\}|cd)/$PWD/

# Set the comment line correctly
s/::/##/g
s/#:/##/g
s/^ *rem/##/g

# Convert echo commands
s/@echo/echo/g
s/echo off//
s/echo[^ ]+//

# Remove cls
s/^cls\s*$//

# Remove blocking pause or nocd
s/(pause|nocd)([.]exe)? *//g

# Replace notepad, explorer, more with cat
s/(notepad|explorer|more)([.]exe)? */cat /g

# Replace start for html, htm, txt with cat
s/(\s*|^)start([.]exe)?\s*(.+[.](html|htm|txt))/\1cat \3/g

# Just display the html files using cat for now
# I'll look at loading in a better browser later
s/^[^ ]*\.html?/cat &/

# Change the dos-style backslash to the unix-style slash
s#\\#/#g

# Linux uses rm, not del
s/^del /rm /

# The dos rename command will rename files without moving them
# the mmv command has an option that does this as well
s/^ren(ame)? /mmv -r /

# use the cp command instead of copy.
s#^copy #cp -fr #

# use cp to replace xcopy as well
s/^xcopy /cp -fr /

# the parameters /y /q /e /s is not required for the copy command
/^cp /s#( +(/y|/q|/e|/s))+##g

# The DOS copy command copies the contents of one directory
# to a new directory by specifying the directory name
# As all files seem to have an extension, and none of the
# directories do, we can convert all such copy commands
# to copy the contents of a directory, instead of the
# directory itself
s#^cp +(.*)/([^\./ ]*) +#cp \1/\2/* #

# replace rd command
s#^rd #rm -rf #

# the parameters /q /s /f is not required for the remove command
/^rm(dir)? /s#( +(/q|/s|/f))+##g

# Some copy commands use xxxx*.* - this is not required and one
# wildcard is easier to handle
s/\*\.\*/*/g

# The linux versions don't end with .exe
s/(mosunpack|tisunpack|weidu|tis2bg2|oggdec|mpg123|snd2acm)\.exe/\1/g

# Use utilites already avaliable in linux
# We don't need the copy commands for them
s/cp.*(mosunpack|tisunpack|weidu|tis2bg2|oggdec|mpg123|snd2acm).*//g

# Do not remove utils since it was not installed locally
s/rm *(mosunpack|tisunpack|weidu|tis2bg2|oggdec|mpg123|snd2acm).*//
s/rm *.*weidu.*//

# Run the linux version not the one supplied with mod
s#.*/(mosunpack|tisunpack|weidu|tis2bg2|oggdec|mpg123|snd2acm)["]?#\1#
s/_weidu/weidu/

# Add -s option to tisunpack call if not present
s/tisunpack/tisunpack -s/
s/tisunpack -s( *(-f|-h|-d|-e|-V)* *-s *)/tisunpack\1/

# Remove -sw option from tis2bg2 call since it present in version 1.1 only
s/(tis2bg.*) -sw( .*|$)/\1\2/

# Quote --traify# option just in case
s/--traify#/"--traify#"/

# replace weidu setup-* calls in tp2
s/^setup-([^ ]+) +(.*) --force-install (.*)$/weidu --tlkout dialog.tlk --ftlkout dialogf.tlk setup-\1.tp2 \2 --force-install \3/
s/^setup-([^ ]+) +(.*) --force-install-list (.*)$/weidu --tlkout dialog.tlk --ftlkout dialogf.tlk setup-\1.tp2 \2 --force-install-list \3/

# Copy commands with only one parameter are not supported - add .
s/^cp( *[^ ]*) *$/cp\1 ./

# I don't think these files have copy commands that use quotes,
# but just in case ...
s/^cp -fr( *".*") *$/cp -fr\1 ./
s/^cp -fr( *'.*') *$/cp -fr\1 ./

# In lines not begining with # ' between non space symbols most probably should be quoted
/^\s*#/!s/([^ ])'([^ ])/\1\\'\2/

# change md to mkdir
s#^md #mkdir #

# change attrib to chmod
s#^attrib -r #chmod u+r #

# The dos copy command will allow batch renaming of files as it copies.
# The mmv -o command will allow this as well. The wildcard in the
# target filename needs to be replaced with #1 (the first wildcard
# used in the source string)
s/^cp -fr *(.*)\*(.*) (.*)\*/mmv -o -d '\1*\2' \3#1/

# Of course, some of the copy commands already had quotes, we now
# need to remove the new ones we added
s/'"/"/g
s/"'/"/g

# replace strange cd .. form
s/cd[.][.]/cd ../

# the parameters /d is not required for the cd command
s%cd +/d +%cd %

# The move command looks like a standard mv (remove the > null)
s/move (.*) (1>|2>|>) *(nul|null)/mv \1/
s/move (.*)/mv \1/

# remove quotes around globbing except in mmv
/mmv /!s%/([^/]*[*][.][^"]{1,})"%"/\1%g

# Deldir should be replaced with rm -fr
s#(^|\s+)call .*/deldir#\1rm -rf#

# The if statements need to be reviewed.
# They seem to need to be split across many lines

# The if exist structure needs to be edited
s/if exist ("[^"]*")(.*)$/if [ -e \1 ]\
then\
  \2\
fi/
s/if exist ([^ ]*)(.*)$/if [ -e \1 ]\
then\
  \2\
fi/

# The if not exist structure also needs fixing
s/if not exist ("[^"]*")(.*)$/if [ ! -e \1 ]\
then\
  \2\
fi/
s/if not exist ([^ ]*)(.*)$/if [ ! -e \1 ]\
then\
 \2\
fi/

# The structure 'if A == B'
s/if *([^ ]*) *== *([^ ]*)(.*)/if [ \1 = \2 ]\
then\
  \3\
fi/
